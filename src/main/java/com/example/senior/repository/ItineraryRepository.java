package com.example.senior.repository;

import com.example.senior.entity.Itinerary;
import com.example.senior.entity.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ItineraryRepository extends JpaRepository<Itinerary, Long> {

}
