package com.example.senior.repository;

import com.example.senior.entity.Port;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface PortRepository extends JpaRepository<Port, Long> {

    Port findByCode(String code);
}
