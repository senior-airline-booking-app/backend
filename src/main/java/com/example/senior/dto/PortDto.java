package com.example.senior.dto;

import com.example.senior.entity.Plane;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PortDto {

    Long id;
    String code;
    String name;
    String longitude;
    String latitude;
    List<PlaneDto> planeList;

}
