package com.example.senior.dto;

import com.example.senior.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetBookings {

    Long flightId;
    Date bookingTime;
    Date startTime;
    Date endTime;
    String departureCode;
    String destinationCode;
    String departureName;
    String destinationName;
    Integer availableSeats;
    Boolean status;



}
