package com.example.senior.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlaneDto {

    Long id;
    String code;
    Integer numberOfSeats;
    Long homePortId;

}
