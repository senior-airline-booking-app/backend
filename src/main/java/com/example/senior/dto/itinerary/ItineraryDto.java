package com.example.senior.dto.itinerary;

import java.util.Date;

import com.example.senior.dto.PlaneDto;
import com.example.senior.dto.PortDto;
import com.example.senior.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItineraryDto {

    Long id;
    PlaneDto plane;
    PortDto departurePort;
    PortDto destinationPort;
    UserDto initializerUser;
    Boolean isShared;
    Boolean isApproved;
    Date startDate;
    Date endDate;

}
