package com.example.senior.dto.itinerary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetItineraries {

    Long flightId;
    String departureCode;
    String destinationCode;
    String departureName;
    String destinationName;
    Integer availableSeats;
    Double price;
    Date duration;

}
