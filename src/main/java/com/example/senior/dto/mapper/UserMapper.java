package com.example.senior.dto.mapper;

import com.example.senior.dto.user.UserCreationDto;
import com.example.senior.dto.user.UserDto;
import com.example.senior.entity.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;
    public UserDto toDto(Users user) {
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .surname(user.getSurname())
                .passportId(user.getPassportId())
                .build();
    }

    public Users toEntity(UserDto userDto) {
        return Users.builder()
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .passportId(userDto.getPassportId())
                .email(userDto.getEmail())
                .build();
    }
    public Users toEntityCreation(UserCreationDto userDto) {
        return Users.builder()
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .passportId(userDto.getPassportId())
                .email(userDto.getEmail())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .build();
    }

    public List<UserDto> listToDto(List<Users> entities) {
        return null;
    }

    public List<Users> listToEntity(List<UserDto> dtos) {
        return null;
    }
}
