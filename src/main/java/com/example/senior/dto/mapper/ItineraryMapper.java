package com.example.senior.dto.mapper;

import com.example.senior.dto.itinerary.ItineraryDto;
import com.example.senior.entity.Itinerary;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ItineraryMapper{

    private final UserMapper userMapper;
    private final PortMapper portMapper;
    private final PlaneMapper planeMapper;

    public ItineraryDto toDto(Itinerary entity) {
        return ItineraryDto.builder()
                .id(entity.getId())
                .destinationPort(portMapper.toDto(entity.getDestinationPort()))
                .departurePort(portMapper.toDto(entity.getDeparturePort()))
                .plane(planeMapper.toDto(entity.getPlane()))
                .isApproved(entity.getIsApproved())
                .isShared(entity.getIsShared())
                .startDate(entity.getStartDate())
                .endDate(entity.getEndDate())
                .initializerUser(userMapper.toDto(entity.getInitializerUser()))
                .build();
    }

    public Itinerary toEntity(ItineraryDto dto) {
        return Itinerary.builder()
                .id(dto.getId())
                .destinationPort(portMapper.toEntity(dto.getDestinationPort()))
                .departurePort(portMapper.toEntity(dto.getDeparturePort()))
                .plane(planeMapper.toEntity(dto.getPlane()))
                .isApproved(dto.getIsApproved())
                .isShared(dto.getIsShared())
                .startDate(dto.getStartDate())
                .endDate(dto.getEndDate())
                .initializerUser(userMapper.toEntity(dto.getInitializerUser()))
                .build();
    }

    public List<ItineraryDto> listToDto(List<Itinerary> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Itinerary> listToEntity(List<ItineraryDto> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
