package com.example.senior.dto.mapper;

import com.example.senior.dto.BookingDto;
import com.example.senior.dto.GetBookings;
import com.example.senior.entity.Booking;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BookingMapper {

    private final UserMapper userMapper;
    private final ItineraryMapper itineraryMapper;

    public BookingDto toDto(Booking entity) {
        return BookingDto.builder()
                .id(entity.getId())
                .bookingTime(entity.getBookingTime())
                .user(userMapper.toDto(entity.getUser()))
                .itinerary(itineraryMapper.toDto(entity.getItinerary()))
                .build();
    }

    public Booking toEntity(BookingDto dto) {
        return Booking.builder()
                .id(dto.getId())
                .bookingTime(dto.getBookingTime())
                .user(userMapper.toEntity(dto.getUser()))
                .itinerary(itineraryMapper.toEntity(dto.getItinerary()))
                .build();
    }

    public GetBookings getBookingsDto(Booking booking) {
        return GetBookings.builder().
                flightId(booking.getItinerary().getId())
                .departureCode(booking.getItinerary().getDeparturePort().getCode())
                .destinationCode(booking.getItinerary().getDestinationPort().getCode())
                .departureName(booking.getItinerary().getDeparturePort().getName())
                .destinationName(booking.getItinerary().getDestinationPort().getName())
                .bookingTime(booking.getBookingTime())
                .flightId(booking.getItinerary().getId())
                .availableSeats(booking.getItinerary().getPlane().getSeatsAvailable())
                .status(booking.getItinerary().getIsApproved())
                .startTime(booking.getItinerary().getStartDate())
                .endTime(booking.getItinerary().getEndDate())
                .build();
    }

    public List<BookingDto> listToDto(List<Booking> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Booking> listToEntity(List<BookingDto> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<GetBookings> listToGetDto(List<Booking> bookings) {
        return bookings.stream().map(this::getBookingsDto).collect(Collectors.toList());
    }
}
