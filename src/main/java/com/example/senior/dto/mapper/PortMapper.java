package com.example.senior.dto.mapper;

import com.example.senior.dto.PortDto;
import com.example.senior.entity.Port;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PortMapper{

    private final PlaneMapper planeMapper;
    public PortDto toDto(Port entity) {
        return PortDto.builder()
                .id(entity.getId())
                .code(entity.getCode())
                .name(entity.getName())
                .latitude(entity.getLatitude())
                .longitude(entity.getLongitude())
                .planeList(planeMapper.listToDto(entity.getPlaneList()))
                .build();
    }

    public Port toEntity(PortDto dto) {
        return Port.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .name(dto.getName())
                .latitude(dto.getLatitude())
                .longitude(dto.getLongitude())
                .name(dto.getName())
                .planeList(planeMapper.listToEntity(dto.getPlaneList()))
                .build();
    }

    public List<PortDto> listToDto(List<Port> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Port> listToEntity(List<PortDto> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
