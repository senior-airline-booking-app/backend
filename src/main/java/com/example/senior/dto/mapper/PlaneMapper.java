package com.example.senior.dto.mapper;

import com.example.senior.dto.PlaneDto;
import com.example.senior.entity.Plane;
import com.example.senior.entity.Port;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class PlaneMapper{
    public PlaneDto toDto(Plane entity) {
        return PlaneDto.builder()
                .id(entity.getId())
                .numberOfSeats(entity.getNumberOfSeats())
                .code(entity.getCode())
                .homePortId(entity.getHomeport().getId())
                .build();
    }

    public Plane toEntity(PlaneDto dto) {
        return Plane.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .numberOfSeats(dto.getNumberOfSeats())
                .homeport(Port.builder()
                        .id(dto.getId())
                        .build())
                .build();
    }

    public List<PlaneDto> listToDto(List<Plane> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Plane> listToEntity(List<PlaneDto> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
