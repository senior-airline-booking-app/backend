package com.example.senior.dto;

import java.util.Date;

import com.example.senior.dto.itinerary.ItineraryDto;
import com.example.senior.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookingDto {

    Long id;
    UserDto user;
    ItineraryDto itinerary;
    Date bookingTime;

}
