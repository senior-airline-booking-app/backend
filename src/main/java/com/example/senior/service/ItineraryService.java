package com.example.senior.service;

import com.example.senior.entity.Itinerary;
import com.example.senior.filter.FilterItinerary;
import java.util.List;

public interface ItineraryService {
    Itinerary saveItinerary(Itinerary itinerary);

    Itinerary findById(Long id);

    List<Itinerary> findAllItineraries(FilterItinerary filterItinerary);

    void deleteById(Long id);

    Itinerary updateItinerary(Itinerary itinerary);
}
