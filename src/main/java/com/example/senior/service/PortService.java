package com.example.senior.service;

import com.example.senior.entity.Port;
import java.util.List;

public interface PortService {
    Port savePort(Port port);
    Port findByCode(String code);
    List<Port> findAllPorts();
    void deleteById(Long id);
    Port updatePort(Port port);


}
