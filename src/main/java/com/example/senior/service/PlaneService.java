package com.example.senior.service;

import com.example.senior.entity.Plane;
import java.util.List;

public interface PlaneService {
    Plane savePlane(Plane plane);

    Plane findById(Long id);

    List<Plane> findAllPlanes();

    void deleteById(Long id);

    Plane updatePlane(Plane plane);
}
