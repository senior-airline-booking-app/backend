package com.example.senior.service;

import com.example.senior.dto.user.UserDto;
import com.example.senior.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    Users findByEmail(String email);

    Page<UserDto> findAllUsers(Pageable pageable);

    void deleteById(Long id);

    UserDto updateUser(UserDto userDto);

    Users saveUser(Users user);
}
