package com.example.senior.service.impl;

import com.example.senior.entity.Itinerary;
import com.example.senior.entity.Plane;
import com.example.senior.filter.FilterItinerary;
import com.example.senior.repository.ItineraryRepository;
import com.example.senior.repository.PlaneRepository;
import com.example.senior.service.PlaneService;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ItineraryServiceImpl implements com.example.senior.service.ItineraryService {

    private final ItineraryRepository itineraryRepository;
    public static final int TWO_DAYS_CONST = 172800000;
    @Override
    public Itinerary saveItinerary(Itinerary itinerary) {
        return itineraryRepository.save(itinerary);
    }
    @Override
    public Itinerary findById(Long id) {
        return itineraryRepository.findById(id).orElse(null);
    }
    @Override
    public List<Itinerary> findAllItineraries(FilterItinerary filterItinerary) {
        List<Itinerary> temp = itineraryRepository.findAll().stream().filter(itinerary -> itinerary.getDeparturePort().getName().equals(filterItinerary.getDeparturePort()))
                .filter(itinerary -> itinerary.getDestinationPort().getName().equals(filterItinerary.getDestinationPort())).collect(Collectors.toList());

        temp = temp.stream().filter(itinerary -> Math.abs(filterItinerary.getExpectedDate().getTime() - itinerary.getStartDate().getTime()) <= TWO_DAYS_CONST).collect(Collectors.toList());
        return temp;
    }
    @Override
    public void deleteById(Long id) {
        itineraryRepository.deleteById(id);
    }
    @Override
    public Itinerary updateItinerary(Itinerary itinerary) {
        return itineraryRepository.save(itinerary);
    }
}
