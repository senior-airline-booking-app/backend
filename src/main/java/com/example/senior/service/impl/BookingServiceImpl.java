package com.example.senior.service.impl;

import com.example.senior.entity.Booking;
import com.example.senior.entity.Itinerary;
import com.example.senior.entity.Users;
import com.example.senior.repository.BookingRepository;
import com.example.senior.repository.ItineraryRepository;
import com.example.senior.repository.UsersRepository;
import com.example.senior.service.BookingService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final UsersRepository usersRepository;


    @Override
    public Booking saveBooking(Booking booking) {
        return bookingRepository.save(booking);
    }


    @Override
    public Booking findById(Long id) {
        return bookingRepository.findById(id).orElse(null);
    }


    @Override
    public List<Booking> findAllBookings(String email) {
        Users user = usersRepository.findByEmail(email);
        return user.getBookings();
    }


    @Override
    public void deleteById(Long id) {
        bookingRepository.deleteById(id);
    }


    @Override
    public Booking updateBooking(Booking booking) {
        return bookingRepository.save(booking);
    }
}
