package com.example.senior.service.impl;

import com.example.senior.entity.Port;
import com.example.senior.repository.PortRepository;
import com.example.senior.service.PortService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PortServiceImpl implements PortService {

    private final PortRepository portRepository;

    @Override
    public Port savePort(Port port) {
        return portRepository.save(port);
    }

    @Override
    public Port findByCode(String code) {
        return portRepository.findByCode(code);
    }

    @Override
    public List<Port> findAllPorts() {
        return portRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        portRepository.deleteById(id);
    }

    @Override
    public Port updatePort(Port port) {
        return portRepository.save(port);
    }
}
