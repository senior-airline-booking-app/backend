package com.example.senior.service.impl;

import com.example.senior.controller.handler.UserException;
import com.example.senior.dto.user.UserDto;
import com.example.senior.dto.mapper.UserMapper;
import com.example.senior.entity.Role;
import com.example.senior.entity.Users;
import com.example.senior.repository.RoleRepository;
import com.example.senior.repository.UsersRepository;
import com.example.senior.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UsersRepository usersRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;
    private final Logger logger = LogManager.getLogger(getClass());
    private final static String USER_ALREADY_EXIST = "There is already an account with the same email.";

    @Override
    public Users findByEmail(String email) {
        logger.debug("Finding the user by email = " + email);
        return usersRepository.findByEmail(email);
    }

    @Override
    public Page<UserDto> findAllUsers(Pageable pageable) {
        logger.debug("Trying to find all users for page " + pageable);
        Page<Users> users = usersRepository.findAll(pageable);
        logger.debug("Found users: " + users);
        return new PageImpl<>(users.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList()));
    }

    @Override
    public void deleteById(Long id) {
        logger.debug("Trying to delete the user with id = " + id );
        usersRepository.deleteById(id);
        logger.debug("Successfully deleted");
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        return null;
    }

    @Override
    public Users saveUser(Users user) {
        logger.debug("Trying to save a user. " + user);
        Users existingUser = this.findByEmail(user.getEmail());

        if(existingUser != null){
            logger.debug("Such user already exists. " + user);
            throw new UserException(USER_ALREADY_EXIST);
        }

        Role role = roleRepository.findByName("ROLE_USER");
        List<Role> list = new ArrayList<>();
        list.add(role);
        user.setRoles(list);
        logger.debug("Saving user.");
        return usersRepository.save(user);
    }
}
