package com.example.senior.service.impl;

import com.example.senior.entity.Plane;
import com.example.senior.repository.PlaneRepository;
import com.example.senior.service.PlaneService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PlaneServiceImpl implements PlaneService {

    private final PlaneRepository planeRepository;


    @Override
    public Plane savePlane(Plane plane) {
        return planeRepository.save(plane);
    }


    @Override
    public Plane findById(Long id) {
        return planeRepository.findById(id).orElse(null);
    }


    @Override
    public List<Plane> findAllPlanes() {
        return planeRepository.findAll();
    }


    @Override
    public void deleteById(Long id) {
        planeRepository.deleteById(id);
    }


    @Override
    public Plane updatePlane(Plane plane) {
        return planeRepository.save(plane);
    }
}
