package com.example.senior.service;

import com.example.senior.entity.Booking;
import java.util.List;

public interface BookingService {
    Booking saveBooking(Booking booking);

    Booking findById(Long id);

    List<Booking> findAllBookings(String email);

    void deleteById(Long id);

    Booking updateBooking(Booking booking);
}
