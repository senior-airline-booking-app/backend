package com.example.senior.controller;

import com.example.senior.dto.PlaneDto;
import com.example.senior.dto.mapper.PlaneMapper;
import com.example.senior.service.PlaneService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class PlaneController {

    private final PlaneService planeService;
    private final PlaneMapper planeMapper;
    private final Logger logger = LogManager.getLogger(getClass());

    @GetMapping(value = "/planes")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<PlaneDto> getPlanes() {
        logger.info("GET request to get planes.");
        return planeMapper.listToDto(planeService.findAllPlanes());
    }

    @DeleteMapping(value = "/planes/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void deletePlane(@Valid @PathVariable(name = "id") Long id) {
        logger.info("DELETE request to delete plane id = {}", id);
        planeService.deleteById(id);
    }

    @PutMapping(value = "/planes")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public PlaneDto updatePlane(@Valid @RequestBody PlaneDto planeDto) {
        logger.info("PUT request to update plane = {}", planeDto);
        return planeMapper.toDto(planeService.updatePlane(planeMapper.toEntity(planeDto)));
    }

    @GetMapping(value = "/planes/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public PlaneDto getPlane(@Valid @PathVariable(name = "id") Long id) {
        logger.info("GET request to get plane by id = {}", id);
        return planeMapper.toDto(planeService.findById(id));
    }

    @PostMapping(value = "/planes")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void addPlane(@Valid @RequestBody PlaneDto planeDto) {
        logger.info("POST request to add plane = {}", planeDto);
        planeService.savePlane(planeMapper.toEntity(planeDto));
    }

}
