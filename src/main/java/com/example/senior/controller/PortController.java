package com.example.senior.controller;

import com.example.senior.dto.PortDto;
import com.example.senior.dto.mapper.PortMapper;
import com.example.senior.service.PortService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class PortController {

    private final PortService portService;
    private final PortMapper portMapper;
    private final Logger logger = LogManager.getLogger(getClass());

    @GetMapping(value = "/ports")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<PortDto> getPorts() {
        logger.info("GET request to get ports.");
        return portMapper.listToDto(portService.findAllPorts());
    }

    @DeleteMapping(value = "/ports/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void deletePort(@Valid @PathVariable(name = "id") Long id) {
        logger.info("DELETE request to delete port id = {}", id);
        portService.deleteById(id);
    }

    @PutMapping(value = "/ports")
    @PreAuthorize("hasAnyRole('ROLE_USER')")
    public PortDto updatePort(@Valid @RequestBody PortDto portDto) {
        logger.info("PUT request to update port = {}", portDto);
        return portMapper.toDto(portService.updatePort(portMapper.toEntity(portDto)));
    }

    @GetMapping(value = "/ports/{code}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public PortDto getPort(@Valid @PathVariable(name = "code") String code) {
        return portMapper.toDto(portService.findByCode(code));
    }

    @PostMapping(value = "/ports")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public PortDto addPort(@Valid @RequestBody PortDto portDto) {
        return portMapper.toDto(portService.savePort(portMapper.toEntity(portDto)));
    }

}
