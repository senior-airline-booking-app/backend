package com.example.senior.controller;

import com.example.senior.dto.user.UserCreationDto;
import com.example.senior.dto.user.UserDto;
import com.example.senior.dto.mapper.UserMapper;
import com.example.senior.service.UserService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final Logger logger = LogManager.getLogger(getClass());

    @GetMapping(value = "/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<UserDto> getUsers(@Valid @PageableDefault(value = 10, page = 0) Pageable pageable) {
        logger.info("GET request to get users. {}", pageable);
        return userService.findAllUsers(pageable);
    }

    @DeleteMapping(value = "/users/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public void deleteUser(@Valid @PathVariable(name = "id") Long id) {
        logger.info("DELETE request to delete user id = {}", id);
        userService.deleteById(id);
    }

    @PostMapping(value = "/users")
    public UserDto saveUser(@Valid @RequestBody UserCreationDto userDto) {
        logger.info("POST request to update user = {}", userDto);
        return userMapper.toDto(userService.saveUser(userMapper.toEntityCreation(userDto)));
    }

    @GetMapping(value = "/login_success")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String loginSucc(){
        return "Succ";
    }

}
