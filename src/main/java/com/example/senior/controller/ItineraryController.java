package com.example.senior.controller;

import com.example.senior.dto.itinerary.ItineraryDto;
import com.example.senior.dto.mapper.ItineraryMapper;
import com.example.senior.filter.FilterItinerary;
import com.example.senior.service.ItineraryService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ItineraryController {

    private final ItineraryService itineraryService;
    private final ItineraryMapper itineraryMapper;
    private final Logger logger = LogManager.getLogger(getClass());

    @GetMapping(value = "/itineraries")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public List<ItineraryDto> getItineraries(@Valid @RequestBody FilterItinerary filterItinerary) {
        logger.info("GET request to get itineraries. filter = {}", filterItinerary);
        return itineraryMapper.listToDto(itineraryService.findAllItineraries(filterItinerary));
    }

    @DeleteMapping(value = "/itineraries/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void deleteItinerary(@Valid @PathVariable(name = "id") Long id) {
        logger.info("DELETE request to delete itinerary id = {}", id);
        itineraryService.deleteById(id);
    }

    @PutMapping(value = "/itineraries")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ItineraryDto updateItinerary(@Valid @RequestBody ItineraryDto itineraryDto) {
        logger.info("PUT request to update itinerary = {}", itineraryDto);
        return itineraryMapper.toDto(itineraryService.updateItinerary(itineraryMapper.toEntity(itineraryDto)));
    }

    @GetMapping(value = "/itineraries/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ItineraryDto getItinerary(@Valid @PathVariable(name = "id") Long id) {
        logger.info("GET request to get itinerary by id = {}", id);
        return itineraryMapper.toDto(itineraryService.findById(id));
    }

    @PostMapping(value = "/itineraries")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void addItinerary(@Valid @RequestBody ItineraryDto itineraryDto) {
        logger.info("POST request to add itinerary = {}", itineraryDto);
        itineraryService.saveItinerary(itineraryMapper.toEntity(itineraryDto));
    }

}
