package com.example.senior.controller;

import com.example.senior.dto.BookingDto;
import com.example.senior.dto.GetBookings;
import com.example.senior.dto.mapper.BookingMapper;
import com.example.senior.service.BookingService;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;
    private final BookingMapper bookingMapper;
    private final Logger logger = LogManager.getLogger(getClass());

    @GetMapping(value = "/bookings")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public List<GetBookings> getBookings(@CurrentSecurityContext(expression="authentication?.name") String username) {
        logger.info(username);
        logger.info("GET request to get bookings.");
        return bookingMapper.listToGetDto(bookingService.findAllBookings(username));
    }

    @DeleteMapping(value = "/bookings/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public void deleteBooking(@Valid @PathVariable(name = "id") Long id) {
        logger.info("DELETE request to delete booking id = {}", id);
        bookingService.deleteById(id);
    }

    @PutMapping(value = "/bookings")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public BookingDto updateBooking (@Valid @RequestBody BookingDto bookingDto) {
        logger.info("PUT request to update booking = {}", bookingDto);
        return bookingMapper.toDto(bookingService.updateBooking(bookingMapper.toEntity(bookingDto)));
    }

    @GetMapping(value = "/bookings/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public BookingDto getBooking(@Valid @PathVariable(name = "id") Long id) {
        logger.info("GET request to get booking by id = {}", id);
        return bookingMapper.toDto(bookingService.findById(id));
    }

    @PostMapping(value = "/bookings")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void addBooking(@Valid @RequestBody BookingDto bookingDto) {
        logger.info("POST request to add itinerary = {}", bookingDto);
        bookingService.saveBooking(bookingMapper.toEntity(bookingDto));
    }

}
