package com.example.senior.entity;

import java.util.List;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "port")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Port {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "code")
    String code;

    @Column(name = "name")
    String name;

    @Column(name = "longitude")
    String longitude;

    @Column(name = "latitude")
    String latitude;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "homeport", cascade = CascadeType.ALL)
    List<Plane> planeList;

}
