package com.example.senior.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "plane")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Plane {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "code")
    String code;

    @Column(name = "number_of_seats")
    Integer numberOfSeats;

    @Column(name = "seats_available")
    Integer seatsAvailable;

    @ManyToOne(cascade = CascadeType.ALL)
    Port homeport;

}
