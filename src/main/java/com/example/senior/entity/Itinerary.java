package com.example.senior.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "itinerary")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Itinerary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    Plane plane;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    Port departurePort;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    Port destinationPort;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    Users initializerUser;

    @Column(name = "is_shared")
    Boolean isShared;

    @Column(name = "is_approved")
    Boolean isApproved;

    @Column(name = "start_time")
    Date startDate;

    @Column(name = "end_time")
    Date endDate;

}
