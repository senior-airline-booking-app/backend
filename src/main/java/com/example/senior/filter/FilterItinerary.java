package com.example.senior.filter;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilterItinerary {

    private String departurePort;
    private String destinationPort;
    private Date expectedDate;

}
